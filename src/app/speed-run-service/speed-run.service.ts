import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';


import { Observable } from 'rxjs';

@Injectable()
export class SpeedRunService {
  speedRunUrl = 'https://www.speedrun.com/api/v1/games';


  constructor(
    private http: HttpClient,
  ) { }

  // get all data
  getAllData(): any {
    return this.http.get(this.speedRunUrl)
      .toPromise();
  }

  // get run data
  getRunData(url): any {
    return this.http.get(url)
      .toPromise();
  }

    // get run data
  getPlayer(url): any {
    return this.http.get(url)
      .toPromise();
  }
}
