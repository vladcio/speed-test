import { TestBed, inject } from '@angular/core/testing';

import { SpeedRunService } from './speed-run.service';

describe('SpeedRunService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpeedRunService]
    });
  });

  it('should be created', inject([SpeedRunService], (service: SpeedRunService) => {
    expect(service).toBeTruthy();
  }));
});
