import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpeedRunService } from '../../speed-run-service/speed-run.service';

@Component({
  selector: 'app-speed-run-details',
  templateUrl: './speed-run-details.view.html',
  styleUrls: ['./speed-run-details.view.css']
})
export class SpeedRunDetailsViewComponent implements OnInit {

  gameId: string;
  gameData: any;
  firstRunData: any;
  dataLink: any;
  allGamesData: any;
  player: any;


  constructor(
    private activatedRoute: ActivatedRoute,
    private speedRunService: SpeedRunService,
  ) { }

  ngOnInit() {
    this.getParams();
    this.getData();
  }

  getdata() {
    this.speedRunService.getAllData()
      .then((data) => {
        this.allGamesData = data;
      });
  }

  getParams() {
    this.activatedRoute.params
    .subscribe(params => {
      this.gameId = params.id;
    });
  }

  getData() {
    this.speedRunService.getAllData()
    .then((data) => {
      this.allGamesData = data.data;
    })
    .then(() => {
      this.getGameSearch(this.allGamesData);
    })
    .then(() => {
      this.getRunData(this.gameData.links);
    });
  }

  getRunData(gameData) {
    this.getRunInfo(gameData)
    .then((res) => {
      this.speedRunService.getRunData(res)
        .then((data) => {
          this.firstRunData = data.data[0];
        })
        .then(() => {
          this.getPlayerData(this.firstRunData.players[0].uri);
        });
      });
  }

  getPlayerData(url) {
    this.speedRunService.getPlayer(url)
    .then((data) => {
      this.player = data.data;
    });
  }

  getRunInfo(gameData): Promise<any> {
    gameData.filter(obj => {
      if (obj.rel === 'runs') {
        this.dataLink = obj.uri;
      }
    });
    return Promise.resolve(this.dataLink);
  }

  getGameSearch(data): Promise<any> {
    data.filter(obj => {
      if (obj.id === this.gameId) {
        this.gameData = obj;
      }
    });
    return Promise.resolve(this.gameData);
  }

  seeVideo() {
    window.open(this.firstRunData.videos.links[0].uri);
  }

  goBack () {
    window.history.back();
  }
}

/**
 * gameData can be sent also by params
 */
// getGameData(): Promise<any> {
//   this.activatedRoute.params
//   .subscribe(params => {
//     this.games = JSON.parse(params.games);
//   });
//   return Promise.resolve(this.gameData.links);
// }
