import { Component, OnInit } from '@angular/core';
import { SpeedRunService } from '../../speed-run-service/speed-run.service';

@Component({
  selector: 'app-speed-run-list',
  templateUrl: './speed-run-list.view.html',
  styleUrls: ['./speed-run-list.view.css']
})
export class SpeedRunListViewComponent implements OnInit {

  data: any;

  constructor(
    private speedRunService: SpeedRunService,
  ) { }

  ngOnInit() {
    this.getdata();
  }
  getdata() {
    this.speedRunService.getAllData()
      .then((data) => {
        this.data = data;
      });
  }
}
