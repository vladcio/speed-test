import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// components
import { AppComponent } from './app.component';
import { SpeedRunListViewComponent } from './pages/speed-run-list/speed-run-list.view';
import { SpeedRunDetailsViewComponent } from './pages/speed-run-details/speed-run-details.view';
import { SpeedRunService } from './speed-run-service/speed-run.service';
import { HttpClientModule } from '@angular/common/http';

// routes
const routes: Routes = [
  {path: '', component: SpeedRunListViewComponent},
  {path: 'details/:id', component: SpeedRunDetailsViewComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    SpeedRunListViewComponent,
    SpeedRunDetailsViewComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  providers: [SpeedRunService],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
